angular.module('app.controllers', ['stripe'])
  
.controller('dealsNearYouCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $http, $state, $cordovaGeolocation) {

	$scope.user = ($rootScope.user);

	$http.get("http://dine-api.appspot.com/item/getAllItems")
		.success(function (response) {
			$scope.items = response;
			angular.forEach($scope.items, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+value.restaurantName)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].restaurantImage = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ value.restaurantName)
				.success(function (response) {
						$scope.items[key].restaurantAddress = response[0].address;
						var options = {timeout: 10000, enableHighAccuracy: true};
							$cordovaGeolocation.getCurrentPosition(options).then(function(position){
							 
							    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

							    $http.get("http://maps.google.com/maps/api/geocode/json?address="+response[0].address).success(function(response) {
							      var destination = (response.results[0].geometry.location);

							      var distance = google.maps.geometry.spherical.computeDistanceBetween(latLng, new google.maps.LatLng(destination.lat,destination.lng)); 
							      	$scope.items[key].distance = distance;
							    });
							    
							});
					}).error(function (response) {
						//console.log(response);
					});
			})
		}).error(function (response) {
			//console.log(response);
		});

	$scope.loadItem = function(item){
		$rootScope.currentItem = item;
		//$state.go('tabsController.item');
		$state.go('tabsController.item', {}, { reload: true })
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

	$scope.loadRestaurant = function(restaurantName){
		$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ restaurantName)
			.success(function (response) {
				$rootScope.currentRestaurant = response[0];
				$state.go('tabsController.restaurant', {}, { reload: true })
			}).error(function (response) {
				//console.log(response);
			});
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};
	
})
   
.controller('highestRatedCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $http) {

	$scope.user = ($rootScope.user);

	$http.get("http://dine-api.appspot.com/item/getPopularItems")
		.success(function (response) {
			$scope.items = response;
			angular.forEach($scope.items, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+value.restaurantName)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].restaurantImage = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ value.restaurantName)
				.success(function (response) {
						$scope.items[key].restaurantAddress = response[0].address;
					}).error(function (response) {
						//console.log(response);
					});
			})
		}).error(function (response) {
			//console.log(response);
		});

		$scope.loadItem = function(item){
		$rootScope.currentItem = item;
		//$state.go('tabsController.item');
		$state.go('tabsController.item', {}, { reload: true })
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

	$scope.loadRestaurant = function(restaurantName){
		$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ restaurantName)
			.success(function (response) {
				$rootScope.currentRestaurant = response[0];
				$state.go('tabsController.restaurant', {}, { reload: true })
			}).error(function (response) {
				//console.log(response);
			});
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

})
   
.controller('popularDealsCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $http) {

	$scope.user = ($rootScope.user);

	$http.get("http://dine-api.appspot.com/item/getRatedItems")
		.success(function (response) {
			$scope.items = response;
			angular.forEach($scope.items, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+value.restaurantName)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.items[key].restaurantImage = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ value.restaurantName)
				.success(function (response) {
						$scope.items[key].restaurantAddress = response[0].address;
					}).error(function (response) {
						//console.log(response);
					});
			})
		}).error(function (response) {
			//console.log(response);
		});

		$scope.loadItem = function(item){
		$rootScope.currentItem = item;
		//$state.go('tabsController.item');
		$state.go('tabsController.item', {}, { reload: true })
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

	$scope.loadRestaurant = function(restaurantName){
		$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ restaurantName)
			.success(function (response) {
				$rootScope.currentRestaurant = response[0];
				$state.go('tabsController.restaurant', {}, { reload: true })
			}).error(function (response) {
				//console.log(response);
			});
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

})
   
.controller('manageMealsCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($rootScope, $scope, $stateParams, $http, $state) {

	$scope.i = {};
	$scope.itemList = {};

	$http.get("http://dine-api.appspot.com/item/getItemsByRestaurantName/"+ $rootScope.user.restaurantName)
	.success(function (response) {
		$scope.itemList = response;
		angular.forEach($scope.itemList, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
						$scope.itemList[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getItemByID/"+value.itemID)
				.success(function (response) {
						$scope.itemList[key].item = response[0];
						
						if(response[0].enabled == true) {
							$scope.itemList[key].enableMeal = {value : 'enabled'};
						}
						else {
							$scope.itemList[key].enableMeal = {value : 'disabled'};
						}
					});

			});

	}).error(function (response) {
		//console.log(response);
	});


	$scope.i.onDelete = function(key,itemID) {
		if($scope.itemList[key].enableMeal.value == "disabled") {
			$http.get("http://dine-api.appspot.com/item/deleteItem/"+itemID)
					.success(function (response) {
							$state.go($state.current, {}, {reload: true});
						});
		}
		else {
			$http.get("http://dine-api.appspot.com/item/enableItem/"+itemID)
					.success(function (response) {
							$state.go($state.current, {}, {reload: true});
						});
		}
	};

	$scope.addNewItem = function() {
		$state.go('newItem',{}, {reload : true});
	};

})
      
.controller('loginCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($rootScope, $scope, $stateParams, $http, $state) {

	$scope.user = {};

    //Go to the guessing page
    $scope.onTouch = function(){
        $emailAddress = ($scope.user.name);
        $password = ($scope.user.password);

       	$http.get("http://dine-api.appspot.com/user/login/"+$emailAddress+"/"+$password)
       	//$http.get("http://dine-api.appspot.com/user/login/abdur.f@gmail.com/abdur")
       	//$http.get("http://dine-api.appspot.com/user/login/shradda.d@gmail.com/shradda")
		.success(function (response) {
			if(response == 401) {
				  $scope.loginStatus = "invalid";
			}
			else {
				$rootScope.user = response[0];

				if(!response.isAdmin) {
					$rootScope.isAdmin = false;
					$state.go('tabsController.dealsNearYou');
				}
				else {
					$rootScope.isAdmin = true;
					if($rootScope.user.restaurantName ==  null) {
						$state.go('restaurantDetails');
					}
					else {
						$state.go('tabsController.manage');
					}
				}

			}
		}).error(function (response) {
			//console.log(response);
			$scope.loginStatus = "invalid";
		});
		
    };

})
   
.controller('signUpCtrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($rootScope, $scope, $stateParams, $http, $state) {
	$scope.i = {};
	$scope.i.fName = '';
	$scope.i.lName = '';
	$scope.i.password = '';
	$scope.i.passwordConf='';
	$scope.i.email = '';
	$scope.i.selectionValue = '';
	$scope.i.image = '';

	$scope.onSignUp = function(){
		$scope.i.pwdError = false;
		$scope.i.fillError = false;
		$scope.i.exists = false;

		if($scope.i.password != $scope.i.passwordConf) {
			$scope.i.pwdError = true;
		}
		else if ($scope.i.image < 1 || $scope.i.selectionValue < 1 || $scope.i.fName.length < 1 || $scope.i.lName.length < 1 || $scope.i.password.length < 1 || $scope.i.email.length < 1) {
			$scope.i.fillError = true;
		}
		else {
			if($scope.i.selectionValue == "customer") {
				$http.get("http://dine-api.appspot.com/user/register/"+$scope.i.fName+"/"+$scope.i.lName+"/"+$scope.i.email+"/"+$scope.i.password+"/"+encodeURIComponent($scope.i.image))
					.success(function (response) {
						if(response == 201) {
							$state.go('signUpSuccessfull', {}, { reload: true });
						}
						else {
							$scope.i.exists = true;
						}
					});
			}
			else if ($scope.i.selectionValue == "restaurant") {
				$http.get("http://dine-api.appspot.com/user/register_admin/"+$scope.i.fName+"/"+$scope.i.lName+"/"+$scope.i.email+"/"+$scope.i.password)
					.success(function (response) {
						if(response == 201) {
							$state.go('signUpSuccessfull', {}, { reload: true });
						}
						else {
							$scope.i.exists = true;
						}
					});
			}
		}
	};

})
   
.controller('signUpSuccessfullCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('searchCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($rootScope, $scope, $stateParams, $http, $state) {

	$scope.search = {};

    //Go to the guessing page
    $scope.onSearch = function(){
        $keyword = ($scope.search.keyword);

       	$http.get("http://dine-api.appspot.com/search/searchdeal/"+ $keyword)
		.success(function (response) {
			$scope.deals = response;
			angular.forEach($scope.deals, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.deals[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+value.restaurantName)
				.success(function (response) {
					//console.log(response[0].link);
						$scope.deals[key].restaurantImage = response[0].link;
					});
			});

		}).error(function (response) {

		});

		$http.get("http://dine-api.appspot.com/search/searchrestaurant/"+ $keyword)
		.success(function (response) {
			$scope.restaurants = response;
			angular.forEach($scope.restaurants, function(value, key) {
				$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+value.restaurantName)
				.success(function (response) {
						$scope.restaurants[key].restaurantImage = response[0].link;
					});
			});

		}).error(function (response) {

		});	
    };

    $scope.loadItem = function(item){
		$rootScope.currentItem = item;
		//$state.go('tabsController.item');
		$state.go('tabsController.item', {}, { reload: true })
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

	$scope.loadRestaurant = function(restaurantName){
		$http.get("http://dine-api.appspot.com/restaurant/getRestaurantByname/"+ restaurantName)
			.success(function (response) {
				$rootScope.currentRestaurant = response[0];
				$state.go('tabsController.restaurant', {}, { reload: true })
			}).error(function (response) {
				//console.log(response);
			});
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};
})
   
.controller('profileCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http) {
	$scope.me = $rootScope.user;

	$http.get("http://dine-api.appspot.com/user/getPurchaseHistory/"+ $scope.me.email)
			.success(function (response) {
				$scope.history = response;
				angular.forEach($scope.history, function(value, key) {
				    $http.get("http://dine-api.appspot.com/item/getItemByID/"+value.itemID)
					.success(function (response) {
					
							$scope.history[key].item = response[0];
						});
					$http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
					.success(function (response) {
						
							$scope.history[key].link = response[0].link;
						});
				});

			}).error(function (response) {
				//console.log(response);
			});

	$scope.viewCode = function(code){
		$rootScope.couponCode = code;
		$state.go('paymentSuccess', {}, { reload: true })
	};
})
   
.controller('historyCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $rootScope) {

	$scope.subTotal = 0;

	$http.get("http://dine-api.appspot.com/item/getSalesList/"+ $rootScope.user.restaurantName)
	.success(function (response) {
		$scope.itemList = response;
		angular.forEach($scope.itemList, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
						$scope.itemList[key].link = response[0].link;
					});
				$http.get("http://dine-api.appspot.com/item/getItemByID/"+value.itemID)
				.success(function (response) {
						$scope.itemList[key].item = response[0];
						$scope.subTotal += value.total*$scope.itemList[key].item.discountPrice;
					});

			});

	}).error(function (response) {
		//console.log(response);
	});
})
   
.controller('itemCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http, $ionicPopup, $cordovaGeolocation) {

	$scope.currentItem = $rootScope.currentItem;
	$scope.inputQ = {};
	$scope.i = {};

	$http.get("http://dine-api.appspot.com/item/getItemRating/"+ $scope.currentItem.itemID)
	.success(function (response) {
		$scope.currentItem.rating = response[0].rating;
	}).error(function (response) {
		//console.log(response);
	});

	$http.get("http://dine-api.appspot.com/item/getItemFeedbackCount/"+ $scope.currentItem.itemID)
	.success(function (response) {
		$scope.currentItem.feedbackCount = response[0].count;
	}).error(function (response) {
		//console.log(response);
	});

	$http.get("http://dine-api.appspot.com/item/getItemFeedbacks/"+ $scope.currentItem.itemID)
	.success(function (response) {
		$scope.currentItemFeedbacks = response;
		angular.forEach($scope.currentItemFeedbacks, function(value, key) {
			$rating = parseInt(value.rating);
			if($rating == 5 || $rating == 4) {
				$scope.currentItemFeedbacks[key].userRating = 'ion-android-star';
				
			}
			else if($rating == 2 || $rating == 3) {
				$scope.currentItemFeedbacks[key].userRating = 'ion-android-star-half';
				
			}
			else {
				$scope.currentItemFeedbacks[key].userRating = 'ion-android-star-outline';
				
			}
		});
	}).error(function (response) {
		//console.log(response);
	});

	$scope.order = function(){
		$qty = $scope.inputQ.qty;
		if($qty > 0) {
			$rootScope.currentOrderItem = $scope.currentItem;
			$rootScope.currentOrderItem.qty = $qty;

			$state.go('payment', {}, { reload: true })
		}
		else {
			$scope.orderStatus = "invalid";
		}
	};

	$scope.submitFeedback = function() {
		if($scope.i.rating > -1 && $scope.i.comment.length > 0) {

			$http.get("http://dine-api.appspot.com/item/isEligibleToFeedback/"+ $rootScope.user.email+"/"+$scope.currentItem.itemID)
			.success(function (response) {
				if(response == 406) {
					$scope.feedbackFailReason = "You should purchase this item before leaving a feedback!";
					$scope.feedbackStatus = "invalid";
				}
				else {
					$couponID = response[0].couponCode;
					$http.get("http://dine-api.appspot.com/item/addFeedBackRating/"+ $couponID+"/"+$scope.i.comment+"/"+$scope.i.rating)
					.success(function (response) {
						var alertPopup = $ionicPopup.alert({
					         title: 'Thank You',
					         template: 'Your Feedback was successfully added'
					      });

					      alertPopup.then(function(res) {
					         // Custom functionality....
					      });

					      $state.reload();

					}).error(function (response) {
						//console.log(response);
					});
				}

			}).error(function (response) {
				//console.log(response);
			});
		}
		else {
			$scope.feedbackFailReason = "Please provide a comment and give a rating for the item";
			$scope.feedbackStatus = "invalid";

		}
	};

	var options = {timeout: 10000, enableHighAccuracy: true};
	$cordovaGeolocation.getCurrentPosition(options).then(function(position){
	 
	    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

	    $http.get("http://maps.google.com/maps/api/geocode/json?address="+$rootScope.currentItem.address).success(function(response) {
	      var destination = (response.results[0].geometry.location);

	      var distance = google.maps.geometry.spherical.computeDistanceBetween(latLng, new google.maps.LatLng(destination.lat,destination.lng)); 
	      if(distance > 1000)
	      	$scope.i.distance = (distance/1000).toFixed(2)+" KMs away from you";
	      else
	      	$scope.i.distance = distance.toFixed(2)+" meters away from you";
	    });
	    
	});

})
   
.controller('restaurantCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http, $cordovaGeolocation) {
	$scope.currentRestaurant = $rootScope.currentRestaurant;
	$scope.i = {};

	var d = new Date();
	var weekday = new Array(7);
	weekday[0]=  "sunday";
	weekday[1] = "monday";
	weekday[2] = "tuesday";
	weekday[3] = "wednesday";
	weekday[4] = "thursday";
	weekday[5] = "friday";
	weekday[6] = "saturday";

	var dd = weekday[d.getDay()];
	var openTime = ($scope.currentRestaurant[dd]);
	var from = openTime.substring(0,4);
	var to = openTime.substring(7,12);
	var current = (d.getHours()+""+("0" + d.getMinutes()).slice(-2));

	if(from <= current && to >= current) {
		$scope.openStatus = "Open Now";
	}
	else {
		$scope.openStatus = "Closed";
	}

	$scope.todayTime = "Today : "+from+" to "+to; 

	$http.get("http://dine-api.appspot.com/item/getItemsByRestaurantName/"+ $scope.currentRestaurant.restaurantName)
	.success(function (response) {
		$scope.itemList = response;
		angular.forEach($scope.itemList, function(value, key) {
			    $http.get("http://dine-api.appspot.com/item/getItemImages/"+value.itemID)
				.success(function (response) {
						$scope.itemList[key].link = response[0].link;
					});
			});

		$http.get("http://dine-api.appspot.com/item/getRestaurantImages/"+$scope.currentRestaurant.restaurantName)
			.success(function (response) {
				//console.log(response[0].link);
					$scope.restaurantImage = response[0].link;
				});

	}).error(function (response) {
		//console.log(response);
	});

	$scope.loadItem = function(item){
		$rootScope.currentItem = item;
		//$state.go('tabsController.item');
		$state.go('tabsController.item', {}, { reload: true })
		//$state.transitionTo('tabsController.item', null, {reload: true, notify:true});
	};

	var options = {timeout: 10000, enableHighAccuracy: true};
	$cordovaGeolocation.getCurrentPosition(options).then(function(position){
	 
	    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

	    $http.get("http://maps.google.com/maps/api/geocode/json?address="+$rootScope.currentRestaurant.address).success(function(response) {
	      var destination = (response.results[0].geometry.location);

	      var distance = google.maps.geometry.spherical.computeDistanceBetween(latLng, new google.maps.LatLng(destination.lat,destination.lng)); 
	      if(distance > 1000)
	      	$scope.i.distance = (distance/1000).toFixed(2)+" KMs away from you";
	      else
	      	$scope.i.distance = distance.toFixed(2)+" meters away from you";
	    });
	    
	});

})
   
.controller('mapCtrl',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $rootScope, $stateParams, $cordovaGeolocation) {

	$scope.i = {};
	$scope.i.loading = true;

	// map object
	  $scope.map = {
	    control: {},
	    center: {
	        latitude: -37.812150,
	        longitude: 144.971008
	    },
	    zoom: 14
	  };
	  
	  // marker object
	  $scope.marker = {
	    center: {
	        latitude: -37.812150,
	        longitude: 144.971008
	    }
	  }

	var options = {timeout: 10000, enableHighAccuracy: true};

	  // instantiate google map objects for directions
	  var directionsDisplay = new google.maps.DirectionsRenderer();
	  var directionsService = new google.maps.DirectionsService();
	  var geocoder = new google.maps.Geocoder();
	 
	 $cordovaGeolocation.getCurrentPosition(options).then(function(position){
	 
	    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	 
	    var mapOptions = {
	      center: latLng,
	      zoom: 15,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	 
	    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
	 	$scope.i.loading = false;

	 	 // directions object -- with defaults
		  $scope.directions = {
		    origin: latLng,
		    destination: $rootScope.currentItem.address,
		    showList: false
		  }

	 	var request = {
	      origin: $scope.directions.origin,
	      destination: $scope.directions.destination,
	      travelMode: google.maps.DirectionsTravelMode.DRIVING
	    };

	    directionsService.route(request, function (response, status) {
	      if (status === google.maps.DirectionsStatus.OK) {
	        directionsDisplay.setDirections(response);
	        directionsDisplay.setMap($scope.map);
	        directionsDisplay.setPanel(document.getElementById('map'));
	        $scope.directions.showList = true;
	        console.log('Google route succesfull!');
	      } else {
	        console.log('Google route unsuccesfull!');
	      }
	    });

	  }, function(error){
	    console.log("Could not get location");
	  });
})
   
.controller('paymentCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http) {
	$scope.i = {};
	$scope.i.stripe = false;

	$item = $rootScope.currentOrderItem;
	$scope.orderItem = $item;
	$scope.i.qty = $item.qty;
	$scope.user = $rootScope.user;

	$scope.total = $item.discountPrice*$item.qty - $scope.user.feedbackPoints/100;

	$scope.qtyChanged = function(){
		$scope.total = $item.discountPrice*$scope.i.qty - $scope.user.feedbackPoints/100;
	};

	$scope.purchase = function(id){
		console.log(id);
		$http.get("http://dine-api.appspot.com/item/order/"+ $rootScope.user.email +"/"+ $item.itemID +"/"+ $scope.i.qty+"/"+id)
		.success(function (response) {
			$state.go('paymentSuccess', {}, { reload: true })
		}).error(function (response) {
			
		});
	};

	$scope.saveCustomer = function(status, response) {
	    if(status != 200) {
	    	$scope.i.stripeError = response.error.message;
	    }
	    else {
	    	$scope.purchase(response.id);
	    }
	  };

	  $scope.purchaseStripe = function() {
	  	if($scope.i.stripe)
	    	$scope.i.stripe = false;
	    else
	    	$scope.i.stripe = true;
	  };

})
   
.controller('paymentSuccessCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http) {

	$scope.last = {};

	if(angular.isUndefined($rootScope.couponCode)) {
		$http.get("http://dine-api.appspot.com/user/getLastPurchase/"+ $rootScope.user.email)
				.success(function (response) {
					$scope.last = response[0];

				}).error(function (response) {
					//console.log(response);
				});
	}
	else {
		$scope.last.couponCode = $rootScope.couponCode;
		delete $rootScope.couponCode;
	}

})

.controller('newItemCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http, $ionicPopup) {

	$scope.i = {};

	$scope.onInsert = function() {
		$http.get("http://dine-api.appspot.com/item/insertItem/"+ $scope.i.name +"/"+ $scope.i.description +"/" +$scope.i.disPrice+"/"+$scope.i.actualPrice+"/"+$rootScope.user.restaurantName)
				.success(function (response) {
					if(response == 201) {
						$http.get("http://dine-api.appspot.com/item/getItemByName/"+ $scope.i.name)
						.success(function (response) {
							$id = response[0].itemID;
							$http.get("http://dine-api.appspot.com/item/insertItemImage/"+ $id+"/"+encodeURIComponent($scope.i.imgLink))
								.success(function (response) {

								});

						});

						var alertPopup = $ionicPopup.alert({
						         title: 'Success',
						         template: 'New Item was successfully added'
						      });

						      alertPopup.then(function(res) {
						         // Custom functionality....
						      });
					}
					else {
						var alertPopup = $ionicPopup.alert({
					         title: 'Oops',
					         template: 'Item Cannot be added. Please try again later'
					      });

					      alertPopup.then(function(res) {
					         // Custom functionality....
					      });
					}
					$state.go('tabsController.manage', {}, { reload: true })
				}).error(function (response) {
					//console.log(response);
				});
	};

	$scope.onCancel = function() {
		$state.go('tabsController.manage', {}, { reload: true })
	};

})
   
.controller('restaurantDetailsCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http, $ionicPopup) {

	$scope.i = {};

	$scope.update = function() {
			$http.get("http://dine-api.appspot.com/restaurant/insertRestaurant/"+ $rootScope.user.email+"/"+$scope.i.name+"/"+encodeURIComponent($scope.i.address)+"/"+$scope.i.desc+"/"
				+$scope.i.paypal+"/"+$scope.i.stripe+"/"+$scope.i.sunday+"/"+$scope.i.monday+"/"+$scope.i.tuesday+"/"+$scope.i.wednesday+"/"
				+$scope.i.thursday+"/"+$scope.i.friday+"/"+$scope.i.saturday)
				.success(function (response) {
					if(response == 201) {
						$http.get("http://dine-api.appspot.com/restaurant/insertRestaurantImage/"+ $scope.i.name +"/"+encodeURIComponent($scope.i.image))
						.success(function (response) {

						}).error(function (response) {
							//console.log(response);
						});

						var alertPopup = $ionicPopup.alert({
					         title: 'Congrats!!',
					         template: 'Your Restaurant is now on AIR! Add deals to get started!'
					      });

					      alertPopup.then(function(res) {
					         // Custom functionality....
					      });

					      $rootScope.user.restaurantName = $scope.i.name;

					    $state.go('tabsController.manage', {}, { reload: true })
					}

				}).error(function (response) {
					//console.log(response);
				});
	};


})
   
.controller('feedbackCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('customerOrderCtrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $rootScope, $http, $ionicPopup) {
	$scope.i = {};
	$scope.orders = {};

	$http.get("http://dine-api.appspot.com/restaurant/getUnCollectedOrders/"+ $rootScope.user.restaurantName)
		.success(function (response) {
			$scope.orders = response

		}).error(function (response) {
			//console.log(response);
		});

	$scope.issueItem = function(couponCode) {

		$http.get("http://dine-api.appspot.com/restaurant/updateIssued/"+ couponCode)
			.success(function (response) {
				var alertPopup = $ionicPopup.alert({
						         title: 'Success',
						         template: 'Item Issued!'
						      });

						      alertPopup.then(function(res) {
						         // Custom functionality....
						      });

				$state.go($state.current, {}, {reload: true});

			}).error(function (response) {
				//console.log(response);
			});
	};

})


 