angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController.dealsNearYou', {
    url: '/dealsNearYou',
    views: {
      'tab3': {
        templateUrl: 'templates/dealsNearYou.html',
        controller: 'dealsNearYouCtrl'
      }
    }
  })

  .state('tabsController.highestRated', {
    url: '/topRated',
    views: {
      'tab5': {
        templateUrl: 'templates/highestRated.html',
        controller: 'highestRatedCtrl'
      }
    }
  })

  .state('tabsController.popularDeals', {
    url: '/popularDeals',
    views: {
      'tab1': {
        templateUrl: 'templates/popularDeals.html',
        controller: 'popularDealsCtrl'
      }
    }
  })

  .state('manageMeals', {
    url: '/meals',
    templateUrl: 'templates/manageMeals.html',
    controller: 'manageMealsCtrl'
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    cache: false,
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signUp', {
    cache: false,
    url: '/signup',
    templateUrl: 'templates/signUp.html',
    controller: 'signUpCtrl'
  })

  .state('newItem', {
    cache: false,
    url: '/newItem',
    templateUrl: 'templates/newItem.html',
    controller: 'newItemCtrl'
  })

  .state('signUpSuccessfull', {
    cache: false,
    url: '/signupSuccess',
    templateUrl: 'templates/signUpSuccessfull.html',
    controller: 'signUpSuccessfullCtrl'
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.search'
      2) Using $state.go programatically:
        $state.go('tabsController.search');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab3/search
      /page1/tab1/search
      /page1/tab5/search
      /page1/tab2/search
  */
  .state('tabsController.search', {
    url: '/search',
    views: {
      'tab3': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      },
      'tab1': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      },
      'tab5': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      },
      'tab2': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      }
    }
  })

  .state('tabsController.profile', {
    cache: false,
    url: '/profile',
    views: {
      'tab4': {
        templateUrl: 'templates/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('tabsController.manage', {
    cache: false,
    url: '/meals',
    views: {
      'tab4': {
        templateUrl: 'templates/manageMeals.html',
        controller: 'manageMealsCtrl'
      }
    }
  })

  .state('tabsController.pendingOrders', {
    cache: false,
    url: '/pendingOrders',
    views: {
      'tab3': {
        templateUrl: 'templates/customerOrders.html',
        controller: 'customerOrderCtrl'
      }
    }
  })

   .state('tabsController.history', {
    cache: false,
    url: '/adminHistory',
    views: {
      'tab2': {
        templateUrl: 'templates/history.html',
        controller: 'historyCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.item'
      2) Using $state.go programatically:
        $state.go('tabsController.item');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab3/item
      /page1/tab1/item
      /page1/tab5/item
      /page1/tab2/item
  */
  .state('tabsController.item', {
    cache: false,
    url: '/item',
    views: {
      'tab3': {
        templateUrl: 'templates/item.html',
        controller: 'itemCtrl'
      },
      'tab1': {
        templateUrl: 'templates/item.html',
        controller: 'itemCtrl'
      },
      'tab5': {
        templateUrl: 'templates/item.html',
        controller: 'itemCtrl'
      },
      'tab2': {
        templateUrl: 'templates/item.html',
        controller: 'itemCtrl'
      }
    }
  })

  .state('tabsController.restaurant', {
    cache: false,
    url: '/restaurant',
    views: {
      'tab3': {
        templateUrl: 'templates/restaurant.html',
        controller: 'restaurantCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.map'
      2) Using $state.go programatically:
        $state.go('tabsController.map');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab3/map
      /page1/tab1/map
      /page1/tab5/map
      /page1/tab2/map
  */
  .state('tabsController.map', {
    cache: false,
    url: '/map',
    views: {
      'tab3': {
        templateUrl: 'templates/map.html',
        controller: 'mapCtrl'
      },
      'tab1': {
        templateUrl: 'templates/map.html',
        controller: 'mapCtrl'
      },
      'tab5': {
        templateUrl: 'templates/map.html',
        controller: 'mapCtrl'
      },
      'tab2': {
        templateUrl: 'templates/map.html',
        controller: 'mapCtrl'
      }
    }
  })

  .state('payment', {
    cache: false,
    url: '/payment',
    templateUrl: 'templates/payment.html',
    controller: 'paymentCtrl'
  })

  .state('paymentSuccess', {
    cache: false,
    url: '/paymentSuccess',
    templateUrl: 'templates/paymentSuccess.html',
    controller: 'paymentSuccessCtrl'
  })

  .state('restaurantDetails', {
    cache: false,
    url: '/restaurantConfig',
    templateUrl: 'templates/restaurantDetails.html',
    controller: 'restaurantDetailsCtrl'
  })

  .state('feedback', {
    cache: false,
    url: '/adminFeedback',
    templateUrl: 'templates/feedback.html',
    controller: 'feedbackCtrl'
  })

$urlRouterProvider.otherwise('/login')

  

});